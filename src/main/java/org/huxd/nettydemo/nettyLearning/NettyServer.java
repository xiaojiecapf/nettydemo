package org.huxd.nettydemo.nettyLearning;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * @ClassName: NettyServer
 * @Description: 服务端启动流程
 * @author: huxudong
 * @Date: 2020/2/13 20:43
 * @Version 1.0
 */
public class NettyServer {
    public static void main(String[] args) {
        /**
         * NioEventLoopGroup 这两个对象可以看做是传统IO编程模型的两大线程组
         * bossGrop 监听端口
         * workeGrop 处理每一条连接的数据读写线程组
         */
        NioEventLoopGroup bossGrop = new NioEventLoopGroup();
        NioEventLoopGroup workeGrop = new NioEventLoopGroup();

        /**
         * serverBooststarp 引导类，这个类引导我们进行服务端的启动工作
         */
        ServerBootstrap serverBootstrap = new ServerBootstrap();
        /**
         * 通过，grop 给引导类两大线程模组，这个引导类模型就搞定类
         */
        serverBootstrap.group(bossGrop, workeGrop)
                // 指定服务端的IO模型为NIO，通过channel 指定IO模型，也可以选择BIO
                .channel(NioServerSocketChannel.class)
                /*
                 * 调用childHandler方法，给引导类创建一个ChannelInitializer 主要就是定义后续每条
                 * 连接的数据读写，业务逻辑。ChannelInitializer中，定义一个范型参数NioSocketChannel
                 * 这个类就是，netty对NIO类型的连接的抽象
                 */
                .childHandler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel nioSocketChannel) throws Exception {

                    }
                });
        /**
         * 如果启动netty服务端，必须指定3类要素，线程模型，IO模型，连续读写逻辑处理
         */

        // 修改为自动绑定递增端口
        /**
         * 这是一个异步方法，调用之后立即返回，返回值是一个ChannelFuture，可以给这个ChannelFuture添加
         * 监听器，可以通过监听端口是否绑定成功
         */
        /*serverBootstrap.bind(8000).addListener(new GenericFutureListener<Future<? super Void>>() {
            @Override
            public void operationComplete(Future<? super Void> future) throws Exception {
                if(future.isSuccess()){
                    System.out.println("绑定端口成功");
                }else{
                    System.out.println("绑定端口失败");

                }
            }
        });*/

        /**
         * 优化后
         */
        bind(serverBootstrap, 1000);
    }

    private static void bind(final ServerBootstrap serverBootstrap , final int port){
        serverBootstrap.bind(port).addListener(future -> {
            if(future.isSuccess()){
                System.out.println("端口[" + port + "]绑定成功！");
            }else{
                System.out.println("端口[" + port + "]绑定失败！");
                bind(serverBootstrap, port + 1);
            }
        });
    }
}
