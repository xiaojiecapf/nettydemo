package org.huxd.nettydemo.nettyLearning;


import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName: NettyClient
 * @Description: netty客户端启动详解
 * @author: huxudong
 * @Date: 2020/2/13 22:53
 * @Version 1.0
 */
public class NettyClient {
    private static final int MAX_RETRY = 5;

    public static void main(String[] args) {
        /**
         * 客户端启动类Booststrap，服务端启动辅助类ServerBootstrap
         */

        NioEventLoopGroup workerGrop = new NioEventLoopGroup();

        Bootstrap bootstrap = new Bootstrap();
        bootstrap
                // 指定线程模型
                .group(workerGrop)
                // 指定线程IO为NIO
                .channel(NioSocketChannel.class)
                // IO处理逻辑
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                    }
                });
        // 建立连接
        /*
         * 调用connect方法进行连接
         * connect 方法两个参数  第一个填写ip/域名，第二个填写端口号
         * 返回结果是Future 方法是异步的，可以监听是否连接成功
         */
       /* bootstrap.connect("juejin.im", 80).addListener(future -> {
            if(future.isSuccess()){
                System.out.println("连接成功！");
            }else{
                System.out.println("连接失败！");
            }
        });*/

       connect(bootstrap,"111", 80,MAX_RETRY);
    }

    /**
     * 抽取重新连接代码
     * @param bootstrap
     * @param host
     * @param port
     */
    private static void connect(Bootstrap bootstrap, String host, int port, int retry) {
        bootstrap.connect(host, port).addListener(future -> {
            if (future.isSuccess()) {
                System.out.println("连接成功!");
            } else if (retry == 0) {
                System.err.println("重试次数已用完，放弃连接！");
            } else {
                // 第几次重连
                int order = (MAX_RETRY - retry) + 1;
                // 本次重连的间隔
                int delay = 1 << order;
                System.err.println(new Date() + ": 连接失败，第" + order + "次重连……");
                bootstrap.config().group().schedule(() -> connect(bootstrap, host, port, retry - 1), delay, TimeUnit
                        .SECONDS);
            }
        });
    }


}
