package org.huxd.nettydemo.oldIo;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @ClassName: IoServer
 * @Description: 传统IO服务端
 * @author: huxudong
 * @Date: 2020/2/13 14:57
 * @Version 1.0
 */
public class IoServer {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8000);
        /**
         * server端创建一个serverSocket来监听8000端口，然后创建一个线程，线程里面
         * 不断调用accept方法，获取新的连接，当获取到新的连接之后，给每个连接创建一个新的线程
         * 这个线程让这个线程负责读取数据，然后，读取数据以字节方流的方式。
         */

        // 接收新的连接线程
        new Thread(() ->{
            while (true){
                try {
                    // 阻塞方法获取新连接
                    Socket socket = serverSocket.accept();
                    // 每个新的连接都创建一个线程，负责读取数据
                    new Thread(() -> {
                        try{
                         int len;
                         byte[] data = new byte[1024];
                         InputStream inputStream = socket.getInputStream();
                         // 按字节流方式读取数据
                             while ((len = inputStream.read(data)) != -1){
                                 System.out.println(new String(data, 0, len));
                             }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }).start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
