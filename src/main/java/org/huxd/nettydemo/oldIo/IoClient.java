package org.huxd.nettydemo.oldIo;


import java.io.IOException;
import java.net.Socket;
import java.util.Date;

/**
 * @ClassName: IoClient
 * @Description: 传统IO客户端
 * @author: huxudong
 * @Date: 2020/2/13 14:58
 * @Version 1.0
 */
public class IoClient {
    public static void main(String[] args) {
        /**
         * 客户端代码相对简单，连接上服务器之后，每隔2秒向服务端发送一个带有时间戳的helloword
         */

        new Thread(() -> {
            try {
                Socket socket = new Socket("127.0.0.1",8000);
                while (true){
                    socket.getOutputStream().write((new Date() + ": hello world").getBytes());
                    Thread.sleep(2000);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }
}
