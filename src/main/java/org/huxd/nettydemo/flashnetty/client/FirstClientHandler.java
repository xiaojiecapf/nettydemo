package org.huxd.nettydemo.flashnetty.client;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.nio.charset.Charset;
import java.util.Date;

/**
 * @ClassName: FirstClientHandler
 * @Description: 客户端业务逻辑
 * @author: huxudong
 * @Date: 2020/2/14 15:50
 * @Version 1.0
 */
public class FirstClientHandler extends ChannelInboundHandlerAdapter {

    /**
     * 这个方法，在客户端连接建立成功之后被调用
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.out.println(new Date() + ": 客户端连接写出数据");

        // 获取数据
        ByteBuf buffer = getByteBuf(ctx);

        // 写数据
        ctx.channel().writeAndFlush(buffer);

    }

    private ByteBuf getByteBuf(ChannelHandlerContext ctx){
        /**
         * 获取二进制抽象ByteBuf
         * 获取bytebuf的内存管理器，这个内存管理器的作用就是分配一个ByteBuf
         */
        ByteBuf buffer = ctx.alloc().buffer();

        // 准备数据，指定字符串的字符集为 utf-8
        byte[] bytes = "你好：".getBytes(Charset.forName("utf-8"));

        // 填充数据到byteBuf
        buffer.writeBytes(bytes);
        return buffer;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ByteBuf byteBuf = (ByteBuf) msg;
        System.out.println(new Date() + ": 客户端读到数据 -> " + byteBuf.toString(Charset.forName("utf-8")));
    }
}
