package org.huxd.nettydemo.flashnetty.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName: NettyClient
 * @Description: netty客户端服务端通信 客户端
 * @author: huxudong
 * @Date: 2020/2/14 13:16
 * @Version 1.0
 */
public class NettyClient {

    /**
     * 重试次数
     */
    private static final int MAX_RETRY = 5;

    /**
     * 地址/域名
     */
    private static final String HOST = "127.0.0.1";

    /**
     * 端口号
     */
    private static final int POST = 8000;

    public static void main(String[] args) {

        NioEventLoopGroup workerGrop = new NioEventLoopGroup();

        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(workerGrop)
                .channel(NioSocketChannel.class)
                // 连接超时时间
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 5000)
                /*
                 * 当设置为true的时候，TCP会实现监控连接是否有效，
                 * 当连接处于空闲状态的时候，超过了2个小时，本地的TCP实现会发送一个数据包给远程的 socket，
                 * 如果远程没有发回响应，TCP会持续尝试11分钟，知道响应为止，如果在12分钟的时候还没响应，
                 * TCP尝试关闭socket连接。
                 */
                .option(ChannelOption.SO_KEEPALIVE, true)
                /*
                 * 禁用nagle算法，true为关闭，通俗讲，如何需要高实时性，有数据就马上发送，设置为
                 * true关闭
                 */
                .option(ChannelOption.TCP_NODELAY, true)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        // 编写业务逻辑
                        ch.pipeline().addLast(new FirstClientHandler());
                    }
                });
        connect(bootstrap,HOST,POST,MAX_RETRY);
    }

    private static void connect(Bootstrap bootstrap, String host, int port ,int retry){
        bootstrap.connect(host, port).addListener(future -> {
            if (future.isSuccess()) {
                System.out.println("连接成功!");
            } else if (retry == 0) {
                System.err.println("重试次数已用完，放弃连接！");
            } else {
                // 第几次重连
                int order = (MAX_RETRY - retry) + 1;
                // 本次重连的间隔
                int delay = 1 << order;
                System.err.println(new Date() + ": 连接失败，第" + order + "次重连……");
                bootstrap.config().group().schedule(() -> connect(bootstrap, host, port, retry - 1), delay, TimeUnit
                        .SECONDS);
            }
        });
    }
}
